## Bookcrud

Bookcrud adalah aplikasi CRUD untuk menyimpan data mengenai buku beserta penulisnya.

Bookcrud dibuat dengan framework Laravel versi 5.2 untuk memenuhi project UAS mata kuliah "Bahasa Pemrograman".

## Demo
Lihat demo online [disini](http://bookcrud.demo.tammam.web.id)

## Anggota Kelompok
- Aditya Gusti Tammam (14.1.03.03.0052)
- Dwy Novia Ulin Nuha (14.1.03.03.0046)
- Muhammad Irfan Aziz (14.1.03.03.0035)
