<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title', 'Home') - Bookcrud</title>

        <!-- Bootstrap CSS -->
        <link href="/bower_resources/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/bower_resources/fontawesome/css/font-awesome.min.css">
        @yield('css')
    </head>
    <body>
        @include('partials.nav')
        <div class="container">
            @yield('content')
        </div>

        <!-- jQuery -->
        <script src="/bower_resources/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="/bower_resources/bootstrap/dist/js/bootstrap.min.js"></script>
        @yield('js')
    </body>
</html>
