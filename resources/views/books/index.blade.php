@extends('app')
@section('title')
Book
@stop
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Book(s)</div>
            <div class="panel-body">
                @include('flash::message')
                @if ($books->count() > 0)
                    <a href="{{ action('BookController@create') }}" class="btn btn-primary">Add Book</a>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Author</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;?>
                            @foreach ($books as $book)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $book->title }}</td>
                                    <td>{{ $book->description }}</td>
                                    <td>${{ $book->price }}</td>
                                    <td>{{ $book->author->name }}</td>
                                    <td class="text-right" style="min-width: 128px;">
                                        {!! Form::open(['method' => 'DELETE', 'action' => ['BookController@destroy', $book->id], 'onsubmit' => "return confirm('Are you sure you want to delete this book?');"]) !!}
                                            <a href="{{ action('BookController@edit', $book->id) }}" class="btn btn-info fa fa-edit"></a>
                                            {!! Form::button('', ['type' => 'submit', 'class' => 'btn btn-danger fa fa-trash']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                <?php $no++;?>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <p class="text-center">
                        No book(s). Please <a href="{{ action('BookController@create') }}">create</a> one !
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
@stop
