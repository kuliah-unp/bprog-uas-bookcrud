@extends('app')
@section('title')
Add Book
@stop
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Add Book</div>

            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <span class="text-danger"><b>Oops !</b></span>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(['action' => 'BookController@store', 'class' => 'form']) !!}
                    <div class="form-group">
                        {!! Form::label('title', 'Title : ') !!}
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('description', 'Description : ') !!}
                        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('price', 'Price : ') !!}
                        {!! Form::number('price', null, ['class' => 'form-control', 'step' => 0.01, 'min' => 0]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('author', 'Author : ') !!}
                        {!! Form::select('author_id', $authors, null, ['class' => 'form-control', 'placeholder' => 'Choose author...']) !!}
                    </div>
                    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']); !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop
