<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Bookcrud</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li class="@if (Request::is('author*'))
                active
            @endif"><a href="{{ action('AuthorController@index') }}">Author CRUD</a></li>
            <li class="@if (Request::is('book*'))
                active
            @endif"><a href="{{ action('BookController@index') }}">Book CRUD</a></li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
