@extends('app')
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading text-center">Welcome</div>

            <div class="panel-body text-center">
                <p><b>Bookcrud</b> adalah aplikasi CRUD untuk menyimpan data mengenai buku beserta penulisnya.</p>
                <p><b>Bookcrud</b> dibuat dengan framework Laravel versi 5.2 untuk memenuhi project UAS mata kuliah "Bahasa Pemrograman".</p>
                <p>Silahkan klik tombol <a href="{{ action('BookController@index') }}" class="btn btn-sm btn-primary"><i class="fa fa-play"></i> Start App</a> untuk masuk ke aplikasi.</p>
                <div class="col-md-6 col-md-offset-3">
                    <h3>Anggota kelompok :</h3>
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="text-align: center;">Nama</th>
                                <th style="text-align: center;">NPM</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Aditya Gusti Tammam</td>
                                <td>14.1.03.03.0052</td>
                            </tr>
                            <tr>
                                <td>Dwy Novia Ulin Nuha</td>
                                <td>14.1.03.03.0046</td>
                            </tr>
                            <tr>
                                <td>Muhammad Irfan Aziz</td>
                                <td>14.1.03.03.0035</td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="{{ action('BookController@index') }}" class="btn btn-lg btn-primary">
                        <i class="fa fa-play"></i>
                        Start App
                    </a>
                    <a href="https://gitlab.com/kuliah-unp/bprog-uas-bookcrud" class="btn btn-lg btn-success">
                        <i class="fa fa-code"></i>
                        Download Source Code
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
