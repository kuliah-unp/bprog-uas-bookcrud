@extends('app')
@section('title')
Edit Author
@stop
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Author</div>

            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <span class="text-danger"><b>Oops !</b></span>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::model($author, ['method' => 'PUT','action' => ['AuthorController@update', $author->id], 'class' => 'form']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Name : ') !!}
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']); !!}
                    <a href="{{ action('AuthorController@index') }}" class="btn btn-default pull-right" style="margin-right: 5px;">Cancel</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop
