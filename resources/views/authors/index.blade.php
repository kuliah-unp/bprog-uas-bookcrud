@extends('app')
@section('title')
Author
@stop
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">Author(s)</div>
            <div class="panel-body">
                @include('flash::message')
                @if ($authors->count() > 0)
                    <a href="{{ action('AuthorController@create') }}" class="btn btn-primary">Add Author</a>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;?>
                            @foreach ($authors as $author)
                                <tr>
                                    <td>{{ $no }}</td>
                                    <td>{{ $author->name }}</td>
                                    <td class="text-right">
                                        {!! Form::open(['method' => 'DELETE', 'action' => ['AuthorController@destroy', $author->id], 'onsubmit' => "return confirm('Are you sure you want to delete this author?');"]) !!}
                                            <a href="{{ action('AuthorController@edit', $author->id) }}" class="btn btn-info fa fa-edit"></a>
                                            {!! Form::button('', ['type' => 'submit', 'class' => 'btn btn-danger fa fa-trash']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                <?php $no++;?>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <p class="text-center">
                        No author(s). Please <a href="{{ action('AuthorController@create') }}">create</a> one !
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
@stop
